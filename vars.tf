variable "cidr" {
  type = map(any)
  default = {
    "vpcrange"        = "10.0.0.0/16"
    "pub-sub-1-cidr"  = "10.0.0.0/27"
    "pub-sub-2-cidr"  = "10.0.0.32/27"
    "priv-sub-1-cidr" = "10.0.0.64/27"
    "priv-sub-2-cidr" = "10.0.0.96/27"
    "dest_cidr_block" = "0.0.0.0/0"
  }
}
variable "resources" {
  type = map(any)
  default = {
    "vpc-name"         = "terra-vpc-01"
    "subnet-public-1"  = "terra-pub-sub-01"
    "subnet-public-2"  = "terra-pub-sub-02"
    "subnet-private-1" = "terra-pri-sub-01"
    "subnet-private-2" = "terra-pri-sub-02"
    "igw"              = "terra-igw"
    "nat"              = "terra-nat"
    "pub-rt"           = "terra-pub-rt"
    "priv-rt"          = "terra-pri-rt"
    "elastic-ip"       = "EIP"
  }
}
variable "location" {
  type = map(any)
  default = {
    "aws_region" = "us-east-1"
    "aws_az1"    = "us-east-1a"
    "aws_az2"    = "us-east-1b"
  }
}
variable "security_group" {
  type = map(any)
  default = {
    "sg_bh" = "sg_bastion_host"
  }
}
variable "security_group_priv" {
  type = map(any)
  default = {
    "sg_priv" = "sg_private_instance"
  }
}
variable "bastion_host" {
  type = map(any)
  default = {
    "bastion_host" = "bastion_host"
  }
}
variable "priv-instance-1" {
  type = map(any)
  default = {
    "priv-instance-1" = "priv-instance-1"
  }
}
variable "priv-instance-2" {
  type = map(any)
  default = {
    "priv-instance-2" = "priv-instance-2"
  }
}
variable "priv-instance-3" {
  type = map(any)
  default = {
    "priv-instance-3" = "priv-instance-3"
  }
}
